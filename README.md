# SpaceMission Activity

Explore key concepts of memory management in .NET through an engaging space exploration theme.

## Objective

Understand how memory allocation works on the Stack and Heap, the role of Garbage Collection, and the differences between passing variables by value and by reference.

## The Mission

You are part of the ground control team preparing for the launch of the Orion spacecraft. Your tasks involve astronaut training, spacecraft preparation, and ensuring the launch pad (memory) is ready for the mission.

## Setup

1. Create a new .NET Console Application named `SpaceMission`.
2. Implement the provided `Spacecraft` class and the `Main` method as per the instructions.
3. Use the provided methods to simulate various aspects of the mission.

## Tasks

1. **Prepare the Astronauts and Spacecraft:**
   - Declare an `int` variable for astronaut count and a `Spacecraft` object.
   - Discuss how these variables are stored in memory (Stack vs. Heap).

2. **Clear the Launch Pad (Garbage Collection):**
   - Create temporary objects to simulate pre-launch activity.
   - Use `GC.Collect()` to clear the memory and compare memory usage before and after.

3. **Astronaut Training and Spacesuit Upgrades (Pass by Value vs. Reference):**
   - Implement methods to simulate astronaut training (pass by value) and spacesuit upgrades (pass by reference).
   - Observe and comment on how changes to variables affect the original data.

## Conclusion

This activity provides a hands-on approach to learning about memory management in .NET, making complex concepts more relatable and easier to grasp through a thematic narrative of space exploration.

## Note

Remember, `GC.Collect()` is used here for demonstration purposes. In real-world applications, the .NET runtime handles garbage collection automatically.
