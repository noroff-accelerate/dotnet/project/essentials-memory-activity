﻿using System;

namespace Noroff.Samples.EssentialsMemoryActivity
{
    class Program
    {
        static void Main(string[] args)
        {
            // Astronaut count (Stack variable)
            int astronautCount = 5;
            Console.WriteLine("Initial Astronaut Count: " + astronautCount);

            // Spacecraft creation (Heap object)
            var spacecraft = new Spacecraft { Name = "Orion" };
            spacecraft.Launch();

            // Simulating launch pad clearing (Garbage Collection)
            CreateTemporaryObjects();
            long memoryBeforeGC = GC.GetTotalMemory(false);
            Console.WriteLine("Memory usage before GC: " + memoryBeforeGC);

            GC.Collect();
            long memoryAfterGC = GC.GetTotalMemory(false);
            Console.WriteLine("Memory usage after GC: " + memoryAfterGC);

            // Astronaut training (Pass by value)
            TrainAstronaut(astronautCount);
            Console.WriteLine("Astronaut Count after training: " + astronautCount);

            // Spacesuit upgrade (Pass by reference)
            int suitUpgradeLevel = 1;
            Console.WriteLine("Spacesuit Upgrade Level before: " + suitUpgradeLevel);
            UpgradeSpacesuit(ref suitUpgradeLevel);
            Console.WriteLine("Spacesuit Upgrade Level after: " + suitUpgradeLevel);
        }

        static void CreateTemporaryObjects()
        {
            for (int i = 0; i < 10000; i++)
            {
                var tempObject = new object();
            }
        }

        static void TrainAstronaut(int astronautId)
        {
            astronautId = 999;
        }

        static void UpgradeSpacesuit(ref int level)
        {
            level++;
        }
    }


    public class Spacecraft
    {
        public string Name { get; set; }

        public void Launch()
        {
            Console.WriteLine(Name + " is launching to space!");
        }
    }

}